function filterBy(fruits, dataType) {
    return fruits.filter(fruits => typeof(fruits) !== dataType);
}

let fruits = [`apples`, 1, `tangerines`, `oranges`, 4, 45, `kiwi`, -1, `melon`];
let dataType = `number`;

console.log(filterBy(fruits, dataType));
